let richPeople, expletives, personality, verbs; // variables for keeping the JSON elements
//empty array for holding the person objects
let forbesMenuoftherich = [];
let tolerableAmountOfrichPeople = 6;
//variables for creating and controlling the timer
let timerStart = 0;
let timerEnd;

function preload() {
  //loading and setting JSON files as variables
  richPeople = loadJSON('Assets/RichPeople.json');
  expletives = loadJSON('Assets/Expletives.json');
  personality = loadJSON('Assets/PersonalityTest.json');
  verbs = loadJSON('Assets/VERBS.json');
}

function setup() {
  createCanvas(windowWidth,windowHeight) // creates canvas
  frameRate(30); //30 framerate for smooth writing

}
// fucntion for creating and filling the list of person objects
function eatTheRich() {
  if(forbesMenuoftherich.length < tolerableAmountOfrichPeople){
    forbesMenuoftherich.push (new Person())
  }
}
//function for removing objects at the bottom of the canvas
function checkLeftovers() {
  for (let i = 0; i < forbesMenuoftherich.length; i++) {
    if(forbesMenuoftherich[i].pos.y > height) {
      forbesMenuoftherich.splice(i,1)
    }
  }
}
// function for controlling the individual person objects in the forbes list
function fillTheMenu() {
  for (let i = 0; i < forbesMenuoftherich.length; i++) {
    forbesMenuoftherich[i].statementMaker();
    forbesMenuoftherich[i].move();
  }
}

function draw() {
  timerEnd = 3000;  // timer, 3 seconds

  background(221, 160, 221);  // purple bg
  fillTheMenu()
  //Timer for making sure the statements dont overlap one another
  if(millis() - timerStart > timerEnd) {
  eatTheRich();
  timerStart = millis();
  }

  checkLeftovers();
}
//the person class, containing all we need to know about the rich
class Person {
  constructor() {
    this.name = richPeople.richPeople[floor(random(richPeople.richPeople.length))].name;                  // assign random name from JSON file to this.name
    this.personality = personality.personality_test[floor(random(personality.personality_test.length))];  // assign random personality from JSON file to this.personality
    this.favoriteVerb = verbs.verbs[floor(random(verbs.verbs.length))].present;                           // assign random verb from JSON file to this.favoriteVerb
    this.expletives = expletives.expletives[floor(random(expletives.expletives.length))];                 // assign random expletive word from JSON file to this.expletives
    this.statement = ""; // variable containing the persons statement
    this.ending = ""; // variable for containing the word endings
    this.pos = new createVector(random(10, (width - 740)), 0);
    this.existense;
  }

  move() {
    this.pos.y += 2;  // moving rule for the statements, add 2 pixels to y position of the statements every frame
  }

  statementMaker() {
    // the following if-statement checks the ending of the expletives word and converts it to plurality if needed
    // grammar rules taken from: https://www.grammarly.com/blog/plural-nouns/

    if(this.expletives.charAt(this.expletives.length - 1) == "o") {
      this.ending = "es"; // if the singular noun ends in ‑o, add ‑es to make it plural
    }
    
    else if(this.expletives.charAt(this.expletives.length - 2) == "u" && this.expletives.charAt(this.expletives.length - 1 == "s")) {
      this.expletives.replace(/us/,"i");  // if the singular noun ends in -us, the plural ending is frequently -i
    }

    else if(this.expletives.charAt(this.expletives.length - 2) == "i" && this.expletives.charAt(this.expletives.length - 1 == "s")) {
      this.expletives.replace(/is/,"es"); //if the singular noun ends in -is, the plural ending is -es
    }

    else if(this.expletives.charAt(this.expletives.length - 2) == "o" && this.expletives.charAt(this.expletives.length - 1 == "n")) {
      this.expletives.replace(/on/,"a");  // if the singular noun ends in -on, the plural ending is -a
    }

    else if(this.expletives.charAt(this.expletives.length - 1) == "ss" || this.expletives.charAt(this.expletives.length - 1) == "h" || this.expletives.charAt(this.expletives.length - 1) == "x" || this.expletives.charAt(this.expletives.length - 1) == "z") {
      this.ending = "es"; // i the singular noun ends in -ss, -sh, -ch, -x, or -z, add ‑es to the end to make it plural
    }

    //at the end its asks if the last character = y dont look, it's horrendous
    else if(this.expletives.charAt(this.expletives.length - 2) == "a" || this.expletives.charAt(this.expletives.length - 2) == "e" || this.expletives.charAt(this.expletives.length - 2) == "i" || this.expletives.charAt(this.expletives.length - 2) == "o" || this.expletives.charAt(this.expletives.length - 2) == "u" && this.expletives.charAt(this.expletives.length - 1 == "y")) {
      this.ending = "s";  // if the singular noun ends in -y and the letter before the -y is a vowel, add an -s to make it plural
    }

    else if(this.expletives.charAt(this.expletives.length - 1) == "y") {
      this.expletives.replace(/y/,"ies"); // if a singular noun ends in ‑y and the letter before the -y is a consonant, change the ending to ‑ies to make the noun plural
    }

    else if (this.expletives.charAt(this.expletives.length - 1) != "s") {
        this.ending = "s";  // make regular nouns plural, we simply add -s to the end
    }

    this.statement = "Hello, I am " + this.name + ".\n" + this.personality + "\nAnd I " + this.favoriteVerb + " all of you poor " + this.expletives + this.ending + ".";  // fill in the strings for the individual statement

    //text styling
    fill(0, 102, 51);
    textSize(25);
    this.existense = text(this.statement,this.pos.x,this.pos.y);  //writes the statement to the canvas on method call
  }
}
