# miniX8: Vocable code

## Title of work: *Rich People are Weird*

[![Image from Gyazo](https://i.gyazo.com/6370f83ffd146ee381cca6f6ee23b4ed.png)](https://gyazo.com/6370f83ffd146ee381cca6f6ee23b4ed)
<br/>
Screenshot of our miniX8

## Links

[Core file](https://gitlab.com/asgerml/aesthetic-programming/-/blob/master/miniX8/Core.js)

[RunMe URL](https://asgerml.gitlab.io/aesthetic-programming/miniX8/)

## Short description
*Rich People are Weird* is an E-lit work that creates ***REAL*** (lol no, they're imaginary) and short 3-line statements from rich people (from the Forbes list of 2014).
Drawing on some of the main elements from "*Taroko Gorge*" made by Nick Montfort [[1]](http://iloveepoetry.org/?p=465), the never-ending flow of absurd statements run down the screen in a slow but steady manner.
The statements will spawn randomly in the top of the screen and there will never be shown more than six statements at once.


## How does it work?
The program is built up using 4 JSON files [[4]](https://github.com/dariusk/corpora/tree/master/data), one containing the names of rich people, one with the questions from a personality test, one with a list of verbs and finally a list of expletives (curse words and such). on each call of the draw function, a Person() is made containing a statement built randomly from these four files in the format of this:
```js
this.statement = "Hello, I am " + this.name + ".\n" + this.personality + "\nAnd I " + this.favoriteVerb + " all of you poor " + this.expletives + this.ending + ".";
```
This statement then forms the existence of the person in the canvas allowing it to appear in the list of ```forbesMenuoftherich``` where the statements are shown and then, flow down over the canvas like a waterfall of profanity, hilarity and at times racismy. This feature is closely related to the games made during minix7 which, thanks to [Cecilie Vedsted](https://gitlab.com/CecilieVedsted/aesthetic-programming-minix/-/tree/master/MiniX7) we were allowed to borrow for this sketch.

Since many of the words in the `expletives` JSON file are nouns in the singular, we created an extensive if-statement to handle the grammar of these nouns, as the defined structure of the statements that the program produce ends with a sentence where the noun should be in plural (this can be seen in the code snippet above, i.e. "`"\nAnd I " + this.favoriteVerb + " all of you poor " + this.expletives + this.ending + "."`")...

## Analysis and articulation
> *Code is constructed from language and can be poetic as the programmer can play with the structure, and experiment with symbols, and the syntactic logic.* [Soon & Cox, p. 147]

The `Person` class is written in a much more traditional code-wise manner, whereas the rest of the program has a more poetic side to it, as the names of the functions/methods are more comedic and have a further intention of being read by a human rather than a computer. This makes it closer to the work Vocable Code (the artwork by Winnie Soon) and is more in line with the idea of codeworks in general, where this is the intention/goal. But, since *Rich People are Weird* uses both writing styles, the traditional and the more poetic, is there a noticeable difference in the way we read the different styles?

For us, there is a difference, the poetic code pieces, become important tone signifiers for the program as a whole. The comedic tone held within the code, can also be applied to the executed product, and without it. The program could come off as meanspirited, as it is putting words in peoples mouths. Moreover, the aesthetic value within the code (as presented in the aesthetics of generative code [[6]](http://generativeart.com/on/cic/2000/ADEWARD.HTM))
is more clear and obvious to the reader, making it more open for a poetic reading. With that said the `Person` class' code also has poetic value, first of all its interesting, considering the tone of the program that the rich people are considered "just" people, when they are presented earlier as objects of food. And in that context the need to know their favoriteVerb and personality becomes odd, as those values are rarely cared about when it comes to our actual food. Secondly reading `this.statement` outloud as if performed by the computer creates a flimsical reading of the computers willingness to simply "picking" statements out of hat, without a thought behind the words within. A critisism often held towards those in power when the time for apology is near.

When the two naming schemes are put side by side, the nonsensical nature of the more poetic style is put to shame, as the same aesthetic value can be extracted from the more classical style, while still keeping the code readability high.


## Inspiration and references
##### Inspiration
* [[1] "Taroko Gorge by Nick Montfort",, byLeonardo Flores, 2012](http://iloveepoetry.org/?p=465)
* [[2] "Yo Dawg, I Hear You Like Taroko Gorge" *(Taroko Gorge "Remixes")*, by Nick Montfort, 2011](https://nickm.com/post/2011/09/yo-dawg-i-hear-you-like-taroko-gorge/)
* [[3] Cecilie Vedsted "*MiniX7*" repository](https://gitlab.com/CecilieVedsted/aesthetic-programming-minix/-/tree/master/MiniX7)

##### References

* [[4] Database with JSON files, by Darius Kazemi](https://github.com/dariusk/corpora/tree/master/data). We have used these pre-made JSON files:
  * https://github.com/dariusk/corpora/blob/master/data/humans/celebrities.json
  * https://github.com/dariusk/corpora/blob/master/data/words/expletives.json
  * https://github.com/dariusk/corpora/blob/master/data/words/verbs.json
  * https://github.com/dariusk/corpora/blob/master/data/psychology/personality_test.json
* [5] Soon, Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186.
* [[6] Cox, Geoff, Alex McLean, and Adrian Ward. "The Aesthetics of Generative Code." Proc. of Generative Art](http://generativeart.com/on/cic/2000/ADEWARD.HTM)
