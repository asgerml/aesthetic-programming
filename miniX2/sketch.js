// skin colors:
let c1 = '#F3E2BF';
let c2 = '#E5BC8D';
let c3 = '#DEA34B';
let c4 = '#C77B48';
let c5 = '#76331E';
let c6 = '#2C0A0A';

let colorArray = [c1, c2, c3, c4, c5, c6];         // array with color values
let smileyArray = [":)", ":P", ":D", ":O", ":("];  // array with smileys

let layerColor = '#E69B8F';  // color for layers
let shownColor;              // start color

function setup() {
  createCanvas(500, 500);
  frameRate(8)
  shownColor = random(colorArray);  // start with random color from array
}

function draw() {
  background(150);

  // text:
  textSize(12);
  fill('blue');
  textStyle(BOLD);
  textAlign(TOP, TOP);
  text("Click on boxes to change skin color " + smileyArray[floor(random(5))], 250, 20);  // prints text + random smiley from array

  // function calls:
  drawBoxes();
  drawFaceBase();
  drawHair();
  drawCharacteristics();
}

// function draws boxes with assigned color (inspiration from Shiffman: https://www.youtube.com/watch?v=cnRD9o6odjk&ab_channel=TheCodingTrain):
function drawBoxes() {
  for (let i = 0; i < 6; i++) {
    fill(colorArray[i]);            // fill boxes with color values from colorArray
    rect(50 + 70 * i, 50, 40, 40);  // draw rectangles with 30 pixels between each of them
  }
}

// function changes skin color of emoji when mouse pressed on boxes:
function mousePressed() {
  if (mouseX > 50 && mouseX < 90 && mouseY > 50 && mouseY < 80) {             // when box 1 clicked, set color to c1
    shownColor = c1;

  } else if (mouseX > 120 && mouseX < 160 && mouseY > 50 && mouseY < 90) {    // when box 2 clicked, set color to c2
    shownColor = c2;

  } else if (mouseX > 190 && mouseX < 230 && mouseY > 50 && mouseY < 90) {    // when box 3 clicked, set color to c3
    shownColor = c3;

  } else if (mouseX > 260 && mouseX < 300 && mouseY > 50 && mouseY < 90) {    // when box 4 clicked, set color to c4
    shownColor = c4;

  } else if (mouseX > 330 && mouseX < 370 && mouseY > 50 && mouseY < 90) {    // when box 5 clicked, set color to c5
    shownColor = c5;

  } else if (mouseX > 400 && mouseX < 440 && mouseY > 50 && mouseY < 90) {    // when box 6 clicked, set color to c6
    shownColor = c6;
  }
}

// function draws the base of emojis' face:
function drawFaceBase() {
  // main ellipses for base
  noStroke();
  fill(shownColor);
  ellipse(340, 330, 150, 190);     // right cheek
  ellipse(160, 330, 150, 190);     // left cheek
  ellipse(250, 400, 140, 100);     // chin

  // ears
  fill(shownColor);
  ellipse(375, 225, 50, 50);       // right ear
  ellipse(125, 225, 50, 50);       // left ear
}

// function draws hair on emoji:
function drawHair() {
  noStroke();
  fill('black');
  ellipse(250, 250, 250, 300);  // main ellipse for hair

  fill(shownColor);
  rect(120, 205, 260, 200);     // skin color overlay on main ellipse
  ellipse(300, 210, 100, 60);   // wavy hairstyle

  drawFaceLayers();             // call drawFaceLayers function for proper layering..

  fill('black');                // wavy hairstyle
  ellipse(200, 190, 110, 75);   //

  // hair around left ear
  beginShape();
  vertex(120, 250);
  vertex(131, 205);
  vertex(155, 200);
  endShape();

  // hair around right ear
  beginShape();
  vertex(368, 200);
  vertex(349, 205);
  vertex(380, 250);
  endShape();
}

// function draws layers on face:
function drawFaceLayers() {
  // layering around ears
  fill(layerColor);            // layer color
  ellipse(125, 225, 20, 20);   // inner ear left
  ellipse(375, 225, 20, 20);   // inner ear right

  // layering double chin
  ellipse(250, 410, 80, 20);   // draw lower layer
  fill(shownColor);
  ellipse(250, 405, 80, 20);

  fill(layerColor);
  ellipse(250, 400, 65, 15);   // draw upper layer
  fill(shownColor);
  ellipse(250, 395, 65, 16);


  // layering left cheek
  fill(layerColor);
  ellipse(170, 355, 65, 50, 4, PI, PI * 2, OPEN);   // draw upper layer ellipse
  fill(shownColor);
  ellipse(165, 350, 65, 70, 4, PI, PI * 2, OPEN);   // draw lower layer ellipse

  // layering right cheek
  fill(layerColor);
  ellipse(330, 355, 65, 50, 4, PI, PI * 2, OPEN);   // draw upper layer ellipse
  fill(shownColor);
  ellipse(335, 350, 65, 70, 4, PI, PI * 2, OPEN);   // draw lower layer ellipse
}

// function draws facial characteristics:
function drawCharacteristics() {
  // eyes
  fill('white');
  ellipse(300, 270, 40, 40);  // right eye
  ellipse(200, 270, 40, 40);  // left eye

  // iris
  fill('brown');
  ellipse(300, 270, 20, 20);  // right iris
  ellipse(200, 270, 20, 20);  // left iris

  // pupils
  fill('black');
  ellipse(300, 270, 10, 10);  // right pupil
  ellipse(200, 270, 10, 10);  // left pupil

  // eyelids
  fill(layerColor);
  arc(300, 260, 40, 40, PI, PI * 2, OPEN);  // right eyelid
  arc(200, 260, 40, 40, PI, PI * 2, OPEN);  // left eyelid

  // eyebrows
  fill('black');
  arc(300, 250, 60, 40, PI, PI * 2, OPEN);  // right eyebrow
  arc(200, 250, 60, 40, PI, PI * 2, OPEN);  // left eyebrow

  // nose
  fill(shownColor);
  stroke(color(layerColor));
  arc(250, 312, 45, 40, 0, PI, OPEN);                  // nose tip
  noFill();
  arc(320, 275, 120, 120, PI * 0.8, PI + QUARTER_PI);  // nose line
  strokeWeight(4);                                     // thicker line for nose and lips

  // mouth
  fill('#AC391C');
  arc(250, 355, 75, 70, 0, PI, CHORD);  // 'CHORD' for upper lip

  // teeth
  fill('white');
  noStroke();
  rect(215, 357, 70, 4);               // upper
  arc(250, 375, 52, 26, 0, PI, OPEN);  // lower
}
