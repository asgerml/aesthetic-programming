# miniX2: Emoji

| ![](videoExample.mp4) |
| :---: |
| *Screen recording of my miniX2* |

## Links

[Sketch file](https://gitlab.com/asgerml/aesthetic-programming/-/blob/master/miniX2/sketch.js)

[RunMe URL](https://asgerml.gitlab.io/aesthetic-programming/miniX2/)

## Description
This week we were asked to "reinvent" emojis and design two ourselves.
I had limited time this weekend so my program is pretty much just one emoji except for a small "everchanging" one in the line of text also.
The actual emoji came to life through use of a large part of the primitive geometric 2D shapes listed in the p5.js reference guide.
It depicts a happy, relaxed, overweight man with no cares in his life.

(This section might sound like a cliché, but ...)
\
... I came up with the idea when I was scrolling through the emojis on my iPhone and realized I could not choose an "overweight" (or skinny for that matter) emoji-person.
There is only one choice when it comes to the most human-like emojis - and that is the elliptic face type which is regular, in the sense that it is neither skinny-looking nor overweight-looking but really just "average/generic" in its facial proportions (with no chin, no jawbone, no crooked nose, no nothing really)...
The more I thought about these human-like emojis I would come to the conclusion that I never really identify with any of them as much as I identify with their gestures and facial expressions.
So I wanted to make something that **some** *maybe* could identify with and also show that just because you lie outside of what some might call the mainstream definition of the a "normal body" it does not mean that there is something wrong with you and your body idiom.
Rather there is something wrong with the people who think it means anything...

##### Functions and syntax
As written somewhere above, I have primarily used the 2D Primitives from the reference guide such as the `ellipse()` and `rect()` functions as well as the `arc()` function (which required some refreshing of my mathematical skills because I used it with radians :D)... I have also thought a lot about the layering of the geometric shapes as it is important to layer them in the correct order...

I have also used two arrays and a for-loop just to try it out in JavaScript and also used the ´mousePressed´ function with inspiration from Shiffman.
It is therefore possible to change the skin color of the emoji with a click on one of the colored boxes in the top of the program (the program automatically generates a random color when the program is opened).
Additionally I have sought to make the main function `function draw()` as short as possible and divide the code in single functions with individual tasks.

## Reflection (short)
I think the program reflects my thoughts pretty well although it is not the most aesthetic piece of art nor the most advanced piece of code.

Personally I think the design of the program is fine - I think I get my point across although there is not really that much to it and I did not do anything complex.
When it comes to the code I feel like it is cleaned up okay at least and that the division of the functions is okay also...

##### References and inspiration
Inspiration:
* [1] Daniel Shiffman - The Coding Train: Shapes & Drawing (https://www.youtube.com/watch?v=c3TeLi6Ns1E&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA)
* [2] p5.js reference guide: Arc (https://p5js.org/reference/#/p5/arc)
* [3] p5.js reference guide: Vertex (https://p5js.org/reference/#/p5/vertex)
* [4] Daniel Shiffman - The Coding Train: While and For Loops (https://www.youtube.com/watch?v=cnRD9o6odjk&ab_channel=TheCodingTrain)

References:
* [5] Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70
* [6] Femke Snelting - Modifying the Universal, MedeaTV, 2016 (https://www.youtube.com/watch?v=ZP2bQ_4Q7DY&ab_channel=MedeaTV)
