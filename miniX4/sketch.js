// Found inspiration in some of the examples from this week,
// e.g. in the example with gender selection on Facebook and from the p5.js reference guide:
// - https://p5js.org/reference/#/p5/createSelect -> some options are disabled
//
// other inspiration:
// https://editor.p5js.org/Mikey316/sketches/HJID_X7Wb
// https://editor.p5js.org/samchasan/sketches/HJIhhM4aW

// list of variables
let botIMG, bubbleIMG, fingerIMG;  // images
let button, dropdown, nameTextfield, ageTextfield, radioW, radioBr, radioBl;  // DOM elements

// preload function loads images
function preload() {
  botIMG = loadImage('assets/bot.png');
  bubbleIMG = loadImage('assets/speechBubble.png');
  fingerIMG = loadImage('assets/pointingFinger.png');
}

function setup() {
  createCanvas(1080, 720);
  background('#96EED6');
  console.log("This is: Frontpage") // for test/debugging

  image(botIMG, width/2 - 150, height/10, 300, 300);  // bot img

  textAlign(CENTER);

  // heading and welcome message
  push();
  textSize(30);
  textStyle(BOLD);
  text("Hello human. I'm IntelliBot.", width/2, 50);
  textSize(20);
  text("I'm a completely neutral bot, trained to make an accurate and unbiased judgement of your character.", width/2, height/1.8);  // this is not true
  text("I know human beings.", width/2, height/1.6);
  pop();

  // guiding text - click on button
  textSize(18);
  text("Please click on         to begin", width/2, height/1.4);  // spacing make room for italic word

  push();
  textStyle(ITALIC);
  text("start", width/2 + 27, height/1.4);
  pop();

  // start button
  button = createButton("Start");
  greenButton();
  button.mousePressed(page1); // go to page1
}

function page1() {
  console.log("This is: Page 1"); // for test/debugging
  newPage();
  talkingBot();

  textAlign(LEFT);

  // headers
  push();
  textSize(25);
  textStyle(BOLD);
  text("Question 1", 50, 50);
  text("Question 2", 50, 175);
  text("Question 3", 50, 300);
  text("Question 4", 50, 425);
  pop();

  // other text
  push();
  fill("red");
  textStyle(ITALIC);
  text("* field is required to proceed", width/1.42, height/6); // text in bubble
  text("*", 560, 95);   // question 1
  text("*", 483, 220);  // question 2
  text("*", 560, 345);  // question 3
  text("*", 465, 465);  // question 4
  pop();

  textSize(20);

  // question 1
  text("What is your name?", 80, 100);
  nameTextfield = createInput();       // string is default value
  nameTextfield.position(380, 83);

  // question 2
  text("What is your gender?", 80, 225);  // only option here will be male/female
  dropdown = createSelect();              // create select menu
  dropdown.position(380, 208);
  dropdown.option("-");
  dropdown.option("Male");
  dropdown.option("Female");
  dropdown.option("Other");
  dropdown.disable("Other");
  dropdown.option("User defined");
  dropdown.disable("User defined");

  // question 3
  text("What is your age?", 80, 350);

  ageTextfield = createInput(); //
  ageTextfield.position(380, 333);

  // question 4
  text("What is your skin color?", 80, 475);  // only option here will be "white"

  push();
  strokeWeight(3);  // surrounding box
  rect(380, 455, 80, 70);

  noStroke();
  fill("#F2EACC");          // "white" skin color
  rect(380, 455, 80, 23);

  fill("#B3894A");          // "brown" skin color
  rect(380, 478, 80, 24);

  fill("#57390C");          // "black" skin color
  rect(380, 502, 80, 23);
  pop();

  radioW = createRadio();   // radio button for "white" skin color
  radioW.position(380, 458);
  radioW.option("White");
  radioW.style("color", "black");

  radioBr = createRadio();  // radio button for "brown" skin color
  radioBr.position(380, 480);
  radioBr.style("color", "brown");
  radioBr.option("Brown");
  radioBr.disable();        // not an option

  radioBl = createRadio();  // radio button for "black" skin color
  radioBl.position(380, 502);
  radioBl.style("color", "white");
  radioBl.option("Black");
  radioBl.disable();        // not an option

  // Confirm data button
  button = createButton("Confirm data"); // cannot make it go to next page on one click!! :((
  greenButton();
  button.mousePressed(inputValues);
}

function page2() {
  console.log("This is: Page 2"); // for test/debugging
  newPage();
  talkingBot();
  image(fingerIMG, width/7, height/4, 250, 250);

  // text in speech buble
  text("I now have an accurate idea\nof you as a person!", width/1.45, height/7);

  // guiding text - click on button
  textSize(18);
  textAlign(CENTER);
  text("Please click on               to see the results", width/2, height/1.4);  // spacing make room for italic word

  push();
  textStyle(ITALIC);
  text("proceed", width/2 - 9, height/1.4);
  pop();

  // proceed button
  button = createButton("Proceed");
  greenButton();
  button.mousePressed(page3); // go to page3
}

function page3() {
  console.log("This is: Page 3"); // for test/debugging
  newPage();
  talkingBot();

  // text in speech bubble
  textAlign(LEFT);
  text("It is almost scary how well I\nknow you, am I right?", width/1.42, height/7);

  // result headers
  push();
  textSize(25);
  textStyle(BOLD);
  text("Your name is:", 50, 50);
  text("You are a:", 50, 175);
  text("You are:", 50, 300);
  text("Your skin color is:", 50, 425);
  pop();

  // display the results
  push();
  textSize(25);
  text(name, 300, 50);                  // displays name input
  text(gender, 300, 175);               // displays chosen gender
  text(age + " years old", 300, 300);   // displays age input
  text(skincolor, 300, 425);            // displays skin color
  pop();

  // try again button
  button = createButton("Try again");
  greenButton();
  button.mousePressed(page1); // redirects to page1
}

function inputValues() {
  name = nameTextfield.value(); // assign input in textfield to 'name' variable
  gender = dropdown.value();    // assign choice of gender to the 'gender' variable
  age = ageTextfield.value();   // assign input in textfield to 'age' variable
  skincolor = radioW.value();

  // conditions: must enter a name, select gender (male/female), 5 < age < 100, and skincolor = white (only option)
  if (name != "" && gender != "-" && age > 5 && age < 100 && skincolor === "White") {  // conditions

    radioW.hide();  // hide the three radio buttons - for some reason they do not dissapear when using 'removeElements()'
    radioBr.hide();
    radioBl.hide();
    page2();        // go to page 2

  } else {

    push()
    fill("red");
    textStyle(BOLD);
    text("Please enter true data about you!", width/8, height/1.2);
    pop();
    console.log("Error - Please enter all required information!!"); // otherwise print error message in console
  }

  // print in console (for my own purposes when testing/debugging)
  console.log("name: " + name + " | "+ "gender: " + gender + " | " + "age: " + age + " | " + "skin color: " + skincolor);
}

// img of bot and speech bubble
function talkingBot() {
  image(botIMG, width/1.5, height/4, 300, 300);
  image(bubbleIMG, width/1.5, height/12, 781/2.6, 341/2.6);
}

// Characteristics of the green buttons
function greenButton() {
  button.style("color", "white");
  button.style("background", "green");
  button.position(width/2 - 40, height - 150);
  button.size(80, 50);
}

// Function makes a "new" page
function newPage() {
  background('#96EED6');  // clears bg
  removeElements();       // Clears DOM elents (for some reason it does not clear radio buttons? This is why I used the hide()-function)
  // console.log("ELements removed"); // for test
}

// draw function not used
// function draw() {
//
// }
