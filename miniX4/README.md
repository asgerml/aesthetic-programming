# miniX4: Data Capture

| ![](screenshot.jpg) |
| :---: |
| Screenshot of my miniX4: *Intellibot* |

## Links

[Sketch file](https://gitlab.com/asgerml/aesthetic-programming/-/blob/master/miniX4/sketch.js)

[RunMe URL](https://asgerml.gitlab.io/aesthetic-programming/miniX4/)

## IntelliBot (*Transmediale submission*)
Welcome to the age of *Datafication*.

As a function of the rapid evolution in technology we have been gifted a lot of amazing things such the internet, social media, smart devices, and many more.
But as we now know, all of these things come with a price - All the data that we produce when we use these otherwise amazing products.
These data are a central part of what Shoshana Zuboff calls the *Surveillance Capitalism* (https://youtu.be/hIXhnWUmMvw), which is the society we live in today.
It is a system that is centered around personal data as a commodity for the tech companies.
The fact of the matter is that it is inevitable to be a part of this economic system if you want to use any of the newer technology.
When you accept the terms of use of these technologies (especially social media) it is almost impossible to keep track of what information that will be stored (and also what it will be used for).
This is essentially quite worrisome for many of us as consumers of these services, as it (for many of us) is almost impossible to live in todays western society without having some kinds of social media accounts and use various of these types of technologies.
Almost everything we do is monitored and stored as data on servers all around the world or in the cloud and then sold for profit.
I think there is an old and well known saying that goes: "*If you are not paying for the product, you are the product*".

As the "*Big Tech*" companies have grown to a significant size in today's life, their influence on societies (and us as individuals) simply cannot be ignored.
This influence has lead to further troubling aspects such as the tech companies limitations on "accepted" and "identifiable" genders (e.g. when you could only choose between male/female on Facebook) and also our societies understanding of race/skin color (which is a relatively big issue in the AI business).

The program I have made tries to show some of these things I have described above.
IntelliBot "knows human beings" and are "trained to make an accurate and unbiased judgement of your character".
This is completely false.
The program are biased by default, just like we have seen in many of the AI systems.
And the program limits your choices, as we have seen on various social media platforms like Facebook.
It is more or less just a pain-in-the-a** program...

## Description and thought process
For this miniX we were asked to make a program that experiments with various data capture input and interactive devices (or DOM elements).
I was quick to decide I did not want to do anything with a microphone or video input - I just wanted to do something relatively simple with the use of some of the basic DOM elements from the reference guide.

As I started to work on the code I decided on making a provocative program, a program where you have to feed it some information but the program does not necessarily allow you to give it the information you want to.
A program that judges you "in advance".
My initial thought was a kind of program that would show that we (people) are not as simple as some of the algorithms try to make us seem like.

##### Functions and syntax
For this exercise the only new syntax I have use are the DOM elements.
It is my first time ever trying them but I think that p5.js makes it really easy to use them.
It is probably the `input` function that is the "coolest" one since it creates a text field in which the user can write (and then you can make the program store the input).
On top of that I think this is the first p5.js program I have made where I have not used the `function draw()`...

## Reflection
I think what we are seeing now is just the start of the age of datafication / the surveillance capitalism.
The development right now is that people are getting more and more aware of this industry in which our user data are being sold for profit, and here in Europe the GDPR law has been introduced which is a step in the right direction in holding the companies accountable and make proper demands on them.
Now though, it is crucial that the discussion of this data capture will become a worldwide theme on the political agenda, as there is a definite need of more, better, and clearer regulations and laws on the matter.

---

#### (Clarifying) comments on feedback
From the feedback I received for this miniX, I can see that I might not have explicitly explained my choice of limiting the inputs of gender and skin color...
These limitations are on purpose.
It is an attempt at criticize companies like Facebook (among other social media / big tech companies) where until (relatively) recently, it has not been possible to indicate whether one identifies as anything other than male/female.
The dropdown menu I have made intentionally forces one to choose between male/female even though other possibilities are listed.
The radio buttons for choosing skin color also does not work intentionally as I also tried to criticize the "white privilege" that have shown to be a big problem in the technological world,  for example in AI.

##### References and inspiration
Inspiration:
* [1] Mikey316 from p5.js web editor (https://editor.p5js.org/Mikey316/sketches/HJID_X7Wb)
* [2] samchasan from p5.js web editor (https://editor.p5js.org/samchasan/sketches/HJIhhM4aW)
* [3] p5.js reference guide: DOM (https://p5js.org/reference/#group-DOM)
* [4] p5.js reference guide: createSelect() (https://p5js.org/reference/#/p5/createSelect)

References:
* [5] Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119
* [6] Shoshana Zuboff, “Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary” https://youtu.be/hIXhnWUmMvw
* [7] Mejias, Ulises A. and Nick Couldry. "Datafication". Internet Policy Review 8.4 (2019). Web. 16 Feb. 2021. https://policyreview.info/concepts/datafication
