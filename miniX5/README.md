# miniX5: A generative program

<img src="screenshot.jpg"  width="300" height="300">
<img src="screenshot1.jpg"  width="300" height="300">
<br/>
<img src="screenshot2.jpg"  width="300" height="300">
<img src="screenshot3.jpg"  width="300" height="300">
<br/>
Screenshots of some patterns generated from the program

## Links

[Sketch file](https://gitlab.com/asgerml/aesthetic-programming/-/blob/master/miniX5/sketch.js)

[RunMe URL](https://asgerml.gitlab.io/aesthetic-programming/miniX5/)

## Description
This week we were asked to design a generative program.
I was a little out of ideas so I looked around on the internet finding different types of generative art as inspiration for my own program.
I found some examples of these kinds of circular shapes/patterns [[1]](https://medium.com/computational-media/week3-parametric-rosewindow-8724b00d1450) and caught myself thinking a similar product would be cool.

The program is simple for the user because it has no interactivity, you just open it and it runs.
It uses 14 frames to create a pattern and then it resets and starts again automatically.
The patterns consist of lines, circles and polygons.
The different elements will take on one out of the three colors that are available.
How the pattern ends up looking depends on chance as much of the program is based on the use of the `random()` function.

The program is heavily inspired by the examples that I have linked to (e.g. the shapes are the same), and also the code for the polygons is copied from this example [[2]](https://github.com/matthewepler/Generative-Design-Systems-with-P5js/blob/master/06_outline_shape/end/helpers.js), though I have made some changes to it as the original code creates only a hexagon.

(Last remark here:) I wanted to make it just like the *Asterisk Painting* (John P. Bell, 2014) [[3]](https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch3_InfiniteLoops/) where the patterns are placed next to each other in rows, but I ended up not doing this as it was a little hard to do...
So I might look at it at a later point in time.

##### Rules
There are a bunch of rules in the program so I am just gonna name a few of them here:
* The program loops forever: It creates a pattern within 14 frames before it resets and starts on a new pattern.
* What elements will be drawn: This is based on a "picker" that defines what will be drawn by using `random()`. This is also to be said for both the coloring and weight of the lines.
 * The chance of what elements will be drawn is uneven: There is a higher chance that the inner lines will be created than there is for the inner circles. This means that the program will draw the inner lines more often than it will draw the inner circles.
* The size of the pattern is random: Again, I have used `random()` to choose the size of the pattern (between 200-800 pixels).

There are many more rules throughout the program, but you can argue that the most telling of the program is the fact that the rules are based on a kind of *controlled randomness*.

##### Functions and syntax
I found out about ternary operators in JavaScript which is basically a shorthand if-else statement.
They work just as they do in C++ where I know them from.
Here is the syntax [[4]](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator):

`condition ? expressionIfTrue : expressionIfFalse`

## Reflection (short)
The following is stated in *Aesthetic Programming* (Cox & Soon, 2020, p. 125-126) about *10 PRINT* [[5]](https://10print.org/):

> *[...] the 10 Print program utilizes randomness to generate unpredictable processes and outcomes that seem random to humans. This “generative” capacity questions the extent of control over the creative process, as the following definition of generative art reveals:*
>> *Generative art refers to any art practice where [sic] artists use a system, such as a
set of natural languages, rules, a computer program, a machine, or other procedural
invention, which is set into motion with some degree of autonomy contributing to or
resulting in a completed work of art.* [6]

Based on this quote/definition it is definitely possible to argue for the program being generative, as it is a piece of software based on rules and with a degree of autonomy (although the level of autonomy is debatable).

A funny thing about a program like this is the question whether the computer (the program) or I as the "programmer" should be considered the creator of the produced patterns.
As the programmer I am the one who has made the rules and set the boundaries for what is possible, but as there is no interactivity I (as the user) do not have any control over the outcome.


##### References and inspiration
Inspiration:
* [1] https://medium.com/computational-media/week3-parametric-rosewindow-8724b00d1450
  * [2] https://github.com/matthewepler/Generative-Design-Systems-with-P5js/blob/master/06_outline_shape/end/helpers.js
  * [3] https://www.youtube.com/user/matthewepler/videos
* [4] https://aesthetic-programming.gitlab.io/book/p5_SampleCode/ch3_InfiniteLoops/
* [5] https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator

References:
* [6] Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.
* [7] Cox, Geoff & Soon, Winnie, Aesthetic Programming - A Handbook Of Software Studies, pp. 121-142, Open Humanities Press 2020
