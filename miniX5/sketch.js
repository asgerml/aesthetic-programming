// inspiration and references can be seen in README.md
// special thanks to Matthew Epler and his videos: https://www.youtube.com/user/matthewepler/videos

// global variables
let patternSize;  // size of the drawn pattern
let sides = 6;    // default number of sides
let colors = [];  // array with colors

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(20);
  frameRate(1.5);

  colors = [color('blue'), color('limegreen'), color('purple')];  // thought these colors looked nice
  patternSize = random(200, 800); // random size between 200 and 800 pixels
  console.log("size of pattern = " + patternSize);

  angleMode(DEGREES); // degrees instead of radians
}

function draw() {
  let pick = random(1); // pick random no. between 0 and 1

  if(pick > 0.4) {      // if no. higher than 0.6
    innerLines();       // draw the inner lines
  }

  if(pick > 0.6) {      // if no. higher than 0.4
    outerShapes();      // draw the outer shapes
  }

  if(pick > 0.9) {      // if no. higher than 0.9
    innerCircles();     // draw the inner circles
  }

  if(frameCount > 14) { // reset after 14 counted frames
    newPattern();
  }

  //console.log("pick = " + pick);
  console.log("frame = " + frameCount);
}

// resets canvas for new pattern to be created
function newPattern() {
  background(20);                 // same as before
  patternSize = random(200, 800); // same as before
  frameCount = 0;                 // reset frameCount

  console.log("size of pattern = " + patternSize);
}

// draws the inner circles of the pattern
function innerCircles() {
  let numCircles = sides;                               // sides of the hexagon (hex = always 6)
  let angle = 360 / numCircles;                         // calculate rotate angle for each circle
  let circleSize = (patternSize / 2) * 0.93;            // found "0.93" is best for fitting the circles inside the outer circle
  let position = (patternSize / 2) - (circleSize / 2);  // calculate x position (the center) of the circle
  let strokeColor = getColor();                         // set strokeColor to random color from the color array

  // I learned you can make "conditional (ternary) operators" in JavaScript, it is just like ternary operator in C++!! See reference in README.md file
  let weight = randSelect() ? 1 : 3;                    // if randSelect = true, then weight = 1, ELSE weight = 3

  noFill();
  stroke(strokeColor);                                  // set stroke to selected color
  strokeWeight(weight);                                 // set strokeWeight to selected weight

  push();
    translate(width/2, height/2);                       // find middle of canvas

    for(let i = 0; i <= numCircles; i++) {              // for-loop that will draw 6 circles
      ellipse(position, 0, circleSize, circleSize);     // draw a circle in calculated position with calculated size
      rotate(angle);                                    // rotate calculated angle
    }
  pop()
}

// draws the inner lines of the pattern
function innerLines() {
  let numSteps = randSelect() ? 8 : 12;                     // determines number of steps from middle of the circle to the border
  let step = (patternSize / 2) / numSteps;                  // defines the length (size) of each step: radius / number of steps
  let startPoint = floor(random(0, numSteps));              // finds starting point for line: line starts at this point
  let stopPoint = floor(random(startPoint, numSteps + 1));  // finds end point for line: line ends at this point. "+1" because of floor rounding down
  let numLines = randSelect() ? sides : sides * 2;          //

  let angle = 360 / numLines;         // finds angle for drawing next line dependent on number of lines (6 or 12)
  let weight = randSelect() ? 2 : 5;  // weight of the lines (either 2 or 5)
  let strokeColor = getColor();       // color the lines with random color from array

  noFill();
  stroke(strokeColor);  // set the weight
  strokeWeight(weight); // set the color

  push();
    translate(width/2, height/2);                       // find the middle
    for(let i = 0; i < numLines; i++) {                 // for-loop that will draw either 6 or 12 lines
      line(startPoint * step, 0, stopPoint * step, 0);  // draw a line in calculated position with calculated length (start and stop points)
      rotate(angle);                                    // rotate calculated angle
    }
  pop();
}

// draws the outer shapes (circle/polygon) of the pattern
function outerShapes(){
  let strokeColor = getColor();       // color the polygon with random color from array
  let weight = randSelect() ? 1 : 3;  // weight will either be 1 or 3
  let polyTrue = randSelect();        // draw a polygon? -> true/false ("yes" or "no")

  noFill();
  stroke(strokeColor);  // set the color
  strokeWeight(weight); // set the weight

  push();
    translate(width/2, height/2);               // find middle of canvas
    if(polyTrue) {                              // if "yes" to draw a polygon:
      polygon(0, 0, patternSize / 2);           // draw the polygon with radius that is half the size of the full pattern
    } else {                                    // if "no", then:
      ellipse(0, 0, patternSize, patternSize);  // draw a circle instead
    }
  pop();
}

// draws the polygon (can either be a hexagon or octagon)
function polygon(xPos, yPos, radius) {
  let numberOfSides = randSelect() ? 6 : 8; // random if it is a hexagon or an octagon
  let angle = 360 / numberOfSides;          // calculate the "rotating" angle

  noFill();

  beginShape();
    for(let i = 0; i < numberOfSides; i++) {                       // for-loop that will draw the polygon
      let myVertex = pointOnCircle(xPos, yPos, radius, i * angle); // finding the points on the circle for the polygon
      vertex(myVertex.x, myVertex.y);                              // and draws lines between these points
    }
  endShape(CLOSE);  // this closes the polygon / draws the last line between two points
}

// finds point on the outer circle and makes it into a vector, which is used for the points of the polygons
function pointOnCircle(xPos, yPos, radius, angle) {
  let x = xPos + radius * cos(angle); // calculates the x coordinate
  let y = yPos + radius * sin(angle); // calculates the y coordinate

  return createVector(x, y);  // return the coordinates
}

// random selector, true/false
function randSelect() {
  if(random(1) > 0.5) { // if no. > 0.5, then:
    return true;        // return "true"
  } else {              // if not, then:
    return false;       // return "false" instead
  }
}

// color picker
function getColor() {
  let rand = floor(random(0, colors.length)); // picks a random color in the array

  return colors[rand];  // return the picked color
}
