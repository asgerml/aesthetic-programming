# Aesthetic-Programming

For class exercises in the Aesthetic Programming course..

## Links to miniX's

* [miniX1](https://gitlab.com/asgerml/aesthetic-programming/-/tree/master/miniX1)
* [miniX2](https://gitlab.com/asgerml/aesthetic-programming/-/tree/master/miniX2)
* [miniX3](https://gitlab.com/asgerml/aesthetic-programming/-/tree/master/miniX3)
* [miniX4](https://gitlab.com/asgerml/aesthetic-programming/-/tree/master/miniX4)
* [miniX5](https://gitlab.com/asgerml/aesthetic-programming/-/tree/master/miniX5)
* [miniX6](https://gitlab.com/asgerml/aesthetic-programming/-/tree/master/miniX6)
* [miniX7](https://gitlab.com/asgerml/aesthetic-programming/-/tree/master/miniX7)
* [miniX8](https://gitlab.com/asgerml/aesthetic-programming/-/tree/master/miniX8)
* [miniX9](https://gitlab.com/asgerml/aesthetic-programming/-/tree/master/miniX9)
* [miniX10](https://gitlab.com/asgerml/aesthetic-programming/-/tree/master/miniX10)

