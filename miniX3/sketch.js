// throbber program

// global variables
let rate = 14;  // for frameRate = 14 frames/sec.
let a;          // used in drawLines

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(rate);
  a = height; // set once on start
}

function draw() {
  background(30, 70); // 70 alpha value creates the illusion that the throbber dissapears
  drawLines();
  drawThrobber();
  writeText();
}

function drawLines() {
  // long line to the left
  line(0, a, width/2, a);
  a -= 8;

  // long line to the right
  line(width, a, width/2, a);
  a -= 8;

  // short line to the left
  line(width/12, a, width/4, a);
  a -= 8;

  // short line to the right
  line(width*0.7, a, width*0.9, a);
  a -= 8;

  // start over when reaching top of window
  if (a < 0) {
    a = height;   // reset a variable
  }
}

// inspiration from https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/blob/master/public/p5_SampleCode/ch3_InfiniteLoops/sketch.js
function drawThrobber() {
  let n = 20; // number of triangles

  push();

  // this is borrowed from the throbber example in the course book
  translate(width/2, height/2);           // translate reconfigures the origin position to the center
  let triangles = 360/n * (frameCount%n); // circle of triangles, "%" is modulo
  rotate(radians(triangles));             // rotate the triangles

  // triangle characteristics
  noStroke();
  fill('lightblue');
  triangle(75, -75, -75, 75, -75, -75);   // coordinates for triangle relative to the center

  pop(); // make sure all of the styles above are "off"
}

function writeText() {
  let timePassed = frameCount/rate; // local variable, calculates program runtime

  textSize(70);
  fill('red');
  textAlign(CENTER);
  text("currently loading...", width/2, height/10);
  text("time passed:    " + floor(timePassed) + "  seconds", width/2, height/1.05);

  push(); // push so it does not affect the above text

  fill('grey')
  textSize(35);
  if (timePassed > 11) {  // print after 11 seconds
    text("throbbers", width*0.3, height*0.2);
  }

  if (timePassed > 19) {  // printer afer 19 seconds
    text("are", width*0.42, height*0.37);
  }

  if (timePassed > 34) {  // print after 34 seconds
    text("the", width*0.65, height*0.57);
  }

  if (timePassed > 47) {  // print after 47 seconds
    text("ultimate", width*0.7, height*0.8);
  }

  fill('red');
  textStyle(BOLD);
  textAlign(CENTER, CENTER);
  if (timePassed > 61) {  // print after 61 seconds
    text("time-wasters", width*0.5, height*0.5);
  }

  pop(); // make sure all of the styles above are "off"
}

// from the reference guide: https://p5js.org/reference/#/p5/windowResized
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);  // fit to window wvery time browser window is resized
}
