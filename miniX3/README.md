# miniX3: Throbber

| ![](screenshot.jpg) |
| :---: |
| *Screenshot of my miniX3* |

## Links

[Sketch file](https://gitlab.com/asgerml/aesthetic-programming/-/blob/master/miniX3/sketch.js)

[RunMe URL](https://asgerml.gitlab.io/aesthetic-programming/miniX3/)

## Description
For this miniX we were asked to design a throbber using loops or any of the transformational functions from the p5.js reference guide.
I initially thought about making a throbber based on Darwin's theory of human evolution where the monkey transforms to a human over a loooooong time.
I would have made it more or less as a classic throbber in the middle of the screen where the monkey would first show - and then the next 'version' and the next 'version' and so on, so the throbber in a sense would scroll through the evolution theory.
Honestly, it turned out to be a bit of a project with the animations sizes, positions and so on, and with a busy weekend and low energy I unfortunately turned to something less testing... :(

| ![](evolution.png) |
| :---: |
| *The animations I would have used...*

I ended up doing something completely different, creating a more classic kind of throbber, also in the middle of the screen but drawn by a rotating triangle as opposed to a circulation of circles (much inspiration from the course book by Winnie Soon and Geoff Cox).
I added some moving lines that move from the bottom of the window and to the top and then some text that appears one word at a time as the program runs.

##### Functions and syntax
I have not made a for- or while-loop, I would have used something like that if I actually has made the evolution-throbber.
Instead I have used the `translate()` function to move the origin of the coordinate system to the center of the window.
I have also used `rotate()` for the actual throbber to rotate the triangle and create the illusion of the throbber.

## Reflection
With my original idea - the evolution throbber - I wanted to address the how the fast the technology has developed and how far we have come from what we started as (monkeys).
I thought it would have been cool if there would be a man with a smartphone in his hand after the man at the computer, then a man with virtual reality headset on, and at last a robot.
This should symbolize how technology and the digital world is ever-growing in our lives and will continue to grow in the future, as it seems right now (until we at some point might turn into actual robots or become one with technology).

The program I ended up doing sort of addresses the thing some companies (for example Instagram) do, where they use a throbber in their software to "enhance the user experience", etc., when it is not actually a necessity.
Instagram for example uses it in their "explore" function or when you scroll through your page/wall.
Every time it happens it is actually (in a sense) a kind of short "waste of time" that supposedly enhances the user experience, but on a quite concrete level (and of course if all software we use in our daily life did this) it could amount to a lot of minutes in total in our lives.
Soon and Cox also note this aspect in their book **Aesthetic Programming**:

"*We might go as far as to say that programming allows for a
time-critical understanding of how technologies play a crucial role in our experience of time...*" (p. 93)

This is the reason why the text on the screen reads "throbbers are the ultimate time-wasters".

Also, the printed text is controlled by the written code which means it will print one word at a time when the program has run for a certain number of seconds.
For the viewer, this will seem like it happens in realtime, when it actually does not.
The program just displays the words at a hardcoded time.
It could also be said, that the throbber is an illusion - it looks like it is moving when in reality there is drawn a new triangle every single frame.
The triangle does not fade either, it is actually just the "layering" of the background because I have some transparency/opacity to it.
It is kind of comparable to movies that is also just a lot of pictures displayed really fast, and then for the viewer it looks like it is moving within the screen.

##### References and inspiration
Inspiration:
* [1] Soon & Cox - Sample code (source code) from the ch. 03 "Infinite Loops" in Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 75-77. (sample code can be found here: https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/blob/master/public/p5_SampleCode/ch3_InfiniteLoops/sketch.js)
* [2] p5.js examples: Linear, moving line (https://p5js.org/examples/motion-linear.html)
* [3] p5.js reference guide: windowResized() (https://p5js.org/reference/#/p5/windowResized)
* [4] Instagram - and their throbber (https://www.instagram.com/)

References:
* [5] Soon, Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96

