# miniX1: Sauron

| ![](sauronScreenshot.jpg) |
| :---: |
| *Screenshot of my miniX1* |

## Links

[Sketch file](https://gitlab.com/asgerml/aesthetic-programming/-/tree/master/miniX1/sketch.js)

[RunMe URL](https://asgerml.gitlab.io/aesthetic-programming/miniX1/) &leftarrow; NB! Contains sound that might be a bit loud if volume is high or you are wearing headphones!
If it is too much you can also just click on the canvas and the sound will disappear... (some browsers, for example Firefox, might automatically block the sound - just allow the site to play the audio and then reload).

## Description
For this miniX I thought I would just try out JavaScript and the p5.js library since it is my first time ever using it and also first time ever coding something visual.
I started out by exploring the p5.js reference guide, just looking at the different functions, reading about them and trying to change the values of the parameters and so on to get a hang of their functionalities and possibilities.
In addition to this I tried to watch some of Shiffman's videos and would also look at other peoples' code examples on the internet just to get some sense of the capabilities of p5.js.

When I started to write some code I would actually just play with it for quite some time:
writing something &rightarrow; compile it and see the results &rightarrow; go back and try some new arguments / new functions &rightarrow; see the changes / new results... pretty much just going back and forth again and again.
I had a hard time deciding what to continue with so I ended up just picking a theme and then try out a bunch of different things.

Eventually I went with a kind of a "Lord of the Rings" theme with the Eye of Sauron.
The eye is moving all around the canvas, bouncing on the "frame".
The program does not really have a purpose other than you could see the Eye of Sauron as a type of metaphor for Big Brother and all of the surveillance going on in the world with the CCTV cameras as well as online (+ on social media), etc...

##### Functions and syntax
By habit, I have commented pretty thoroughly in the code (do not know if it is maybe too much?)...

I tried working with a bunch of different functions.
I wanted to try importing an image for the background as well as having some kind of audio play.
I found an image and used the `loadImage()` function to assign it to the `bg` variable and then use the `background(bg)` to set it as the background.
The process was pretty much the same for the audio where I used the `createAudio()` function and then `.play()`.

I went on to make the eye which consists of three ellipses on top of each other.
I thought it would be cool if the eye could move around the canvas so I started to look at how it could be done and found some inspiration in one of Shiffman's videos and further searching - which led to the `movingRule()` function.

The last touch was adding the text and then I decided to add a way of stopping the audio which I found inspiration for in the reference guide...

I feel I have learned some of the basics of JavaScript and found some things I want to look further into - for example some of the sound/audio features (in the p5.sound library) could be interesting to try out.

## Reflection

**What are the differences between reading other people code and writing your own code. What can you learn from reading other works?**
* I think reading other people's code is one of the best ways to learn how to write your own code, of course in combination with you actually doing it.
When you read other people's code you can for example find inspiration, or ways to fix problems in your own code, or maybe learn something entirely new when reading in a textbook (if you watch Shiffman's videos you are also pretty much reading his code so it is a great way of learning - but you will never truly learn if you do not write code yourself).

**What's the similarities and differences between writing and coding?**

* Similarities:
  * Both require learning one or more languages.
  * Both have specific "rules" in the form of syntax and grammar (one could argue).


* Differences:
  * I guess you can say that much of code is understood through symbols such as "||" as well as specific syntax and statements like "if-else" etc. whereas writing is the use of an alphabet to construct sentences.
  * Regular writing can often feel a bit more messy compared to code, while most (good) code is really well structured and with elaborative commenting.

**What is programming and why you need to read and write code (a question relates to coding literacy - check Annette Vee's reading)?**
* Vee's point in the text is by and large that programming is the new hot literacy (or the literacy of the future).
Vee's point in the text is by and large that programming is the new hot literacy (or the literacy of the future).
By juxtaposing programming with reading/writing, Vee argues that programming is just as important for you (and society) as basic skills like reading/writing.
I do not think this comparison is fair because in todays world you miss out on A LOT of things if you cannot read/write which just cannot be said to be the same for programming.
Of course it is true that you might have to learn programming or at least get some knowledge about it in order to understand how much of todays technology/systems/works, and especially if you have any ambitions of changing how things are or want to affect how things should be in the future; society, privacy (or lack thereof), human rights, values etc....

##### References and inspiration
Inspiration:
* [1] Daniel Shiffman - The Coding Train: Bouncing ball (https://www.youtube.com/watch?v=LO3Awjn_gyU&ab_channel=TheCodingTrain)
* [2] ellacyt from p5.js web editor (https://editor.p5js.org/icm/sketches/BJKWv5Tn)

References:
* [3] Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48
* [4] Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93
