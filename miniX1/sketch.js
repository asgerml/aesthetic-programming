// minix1 exercise for the aesthetic programming course.
// was fooling around but ended up doing this 'lord of the rings' thing.
// found inspiration in Shiffman's videos as well as in other
// peoples work: ellacyt on p5.js web editor,
// but mainly in the reference guide.

// list of variables
let bg;                     // for background img
let x = 500;                // x starting position
let y = 207;                // y starting position
let r = 70;                 // radius for ellipses
let horizontalSpeed = 4;    // horizontal speed
let verticalSpeed = 3;      // vertical speed
let sauronSound;            // for mp3

// preload function loads img and audio before setup function runs
function preload() {
  bg = loadImage('assets/mordor.png');
  sauronSound = createAudio('assets/sauronSound.mp3');
}

// canvas and background setup + play sound
function setup() {
  let canvas = createCanvas(1000, 414);         // equal to img size
  print("he really wants his ring");

  sauronSound.volume(0.3);                      // lower volume
  sauronSound.play();                           // play once when site opens
  canvas.mousePressed(canvasPressed);           // calls canvasPressed if you press on canvas
}

// function that stops sound (inspiration from reference guide)
function canvasPressed() {
    sauronSound.stop();
}

// text on screen and creation of the eye
function draw() {
  background(bg);

  textSize(60);
  fill('red');

  textStyle(BOLD);
  text("Sauron is looking for", 100, height/2);         //
  textAlign(LEFT, CENTER);                              // position for text

  textStyle(BOLDITALIC);                                // last word is bold/italic
  text("you", 720, height/2);                           // align with other text
  textAlign(LEFT, CENTER);                              //

  textSize(16);                                         // smaller size for this text
  textStyle(NORMAL);
  text("Click on canvas to shut up Sauron!", 750, 18);

  sauronsEye();    // make eye appear
}

// function creates the eye
function sauronsEye() {
  fill('yellow');                // yellow ellipse in the eye
  ellipse(x, y, r*2, r*2);       // size of ellipse relative to r

  fill('red');                   // red ellipse
  ellipse(x, y, r*1.6, r*1.9);   // position inside of yellow iris

  fill('black');                 // black ellipses
  ellipse(x, y, r/3, r*1.7);     // position inside of red iris

  movingRule();                  // applying rules for the ellipses moving
}

// function defines movement of ellipses (reference: https://editor.p5js.org/icm/sketches/BJKWv5Tn)
function movingRule() {
  x += horizontalSpeed;                     // moving horizontally by adding horizontalSpeed per frame
  y += verticalSpeed;                       // moving vertically by adding verticalSpeed per frame

  if (x > width - r || x < r) {             // making sure they 'bounce' at the sides of the frame
    horizontalSpeed = -horizontalSpeed;     //
  }

  if (y > height - r || y < r) {            // making sure they 'bounce' at top and bottom of frame
    verticalSpeed = -verticalSpeed;         //
  }
}
