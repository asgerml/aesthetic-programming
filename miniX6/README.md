# miniX6: Revisit the past (miniX2 rework)

<img src="screenshot.jpg"  width="300" height="300">
<img src="screenshot1.jpg"  width="300" height="300">
<br/>
Screenshots of my miniX6

## Links

[Sketch file](https://gitlab.com/asgerml/aesthetic-programming/-/blob/master/miniX6/sketch.js)

[RunMe URL](https://asgerml.gitlab.io/aesthetic-programming/miniX6/)

## Description
I was in doubt about how I was to use critical making in the rework of my miniX's, because I had worked in a very different way (much more goal oriented) before this miniX.
For all the other exercises I started out by making a plan for a program within the frames of what the description defined.
This time I will try something new.

As I am writing this I have not chosen what I will do with this exercise; there is no goal yet, I have not chosen which of my previous exercises I will rework, and I do not know what point I will try to prove.
I feel like this approach in some way supports the theme of this week: Critical Making.
As Ratto & Hertz describe (Ratto & Hertz, 2019, p. 24), it is not the *object* that is the main outcome:
> *[...] though objects are produced
through such processes, these objects are not the main outcome. Instead, the intended results
of these experiences are personal, sometimes idiosyncratic transformations in a participant's
understanding and investment regarding critical/conceptual issues.* [1]

Also, I now see the similarities between this course (Aesthetic programming) and how Ratto describes his way of teaching his class in critical making at the University of Toronto (Ratto & Hertz, 2019, p. 24).
The construction of the two classes sounds very much alike...

Well.. Back on track... I have to choose what miniX to rework...

I have chosen to rework my miniX2 where I made an "overweight man"-emoji.
The original reason why I made this was because (and I quote):
> *I came up with the idea when I was scrolling through the emojis on my iPhone and realized I could not choose an "overweight" (or skinny for that matter) emoji-person.
There is only one choice when it comes to the most human-like emojis - and that is the elliptic face type which is regular, in the sense that it is neither skinny-looking nor overweight-looking but really just "average/generic" in its facial proportions (with no chin, no jawbone, no crooked nose, no nothing really)...* [[2]](https://gitlab.com/asgerml/aesthetic-programming/-/tree/master/miniX2)

As I sit and read the code and README for miniX2 again, I start to think about the point of view I had (and the arguments I made).
Since online chatting became popular, emojis have evolved from simple smiley faces made of characters into the emojis we know today.
They have become these advanced "mini drawings" that depicts something from the real world, like humans, animals, food, and so on.

Nowadays the biggest problem with the emojis have shown to be the fact that a lot of people do not identify with them and that they exclude minorities.
There have been big political discussions about differentiation in the world of emojis; for example over the races/skin colors of emojis, but also over things like hair color and other features of appearance.
This has led to a countless number of emojis which keep on increasing as more and more possibilities are introduced as a result of the people of the world expand their horizons and start accepting more types of people.
But the problem seems never-ending as new scandals keep coming because people still do not identify with the emojis and also because of the "symbolism" some of the emojis has been assigned.

I therefore suggest we turn away from the emojis as we know them today and instead go back to the good old smiley faces made of characters that no people identify with as they do not look like "real" people.
With these smiley faces, there are no problem with race/skin color, different body ideals/builds, hair colors and other characteristics of appearance.

To me, this could be a way to stop excluding minorities and instead start including everyone as no one will be left behind.
I do not know how it would work in reality but I think the world would be a simpler place without emojis as they have become way too political and biased.

I have made no direct changes in my program, but I have made some additions.
The program now has a second "page".
If the box in the bottom right corner is clicked, the emoji (human looking) will disappear and an old smiley face will appear with some additional text emphasizing the point of this README...
In that sense I feel like this exercise has been more about the thought process and the conceptual thinking.

## Short reflection (on process and previous work)
I had a great talk with our instructor (Mads) this week in our instructor class.
We had a discussion in a breakout room about critical making and how we could integrate the approach in our miniX's, as I was very much aware about the fact that my earlier approaches had been mostly goal oriented which confused me about how I could solve this week's exercise.

After some in depth explanation/conversation about my previous miniX's it became clear to me (with the help of Mads) that although I have previously worked in a more goal oriented fashion, I have in hindsight used some of the key aspects of critical making anyway.

There really are not any of my miniX's that are directly focused on usability and functionality.
All of them (as stated in my *README*s) should be viewed as critical comments on something within the themes of the weeks in question.
And I make these comments because of my critical thinking within the boundaries of the exercises in relation to the themes of the weeks.
In my understanding, this is also arguably an aspect of critical making because it, as Ratto & Hertz states, builds upon earlier critical approaches such as critical thinking...

##### References
* [1] Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. *The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures*, Amsterdam, 2019, pp. 17-28.
* [[2] My original miniX2](https://gitlab.com/asgerml/aesthetic-programming/-/tree/master/miniX2)
* [3] Soon Winnie & Cox, Geoff, "Preface", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 12-24.
