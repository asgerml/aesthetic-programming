// we use the Fakter API. There's no KEY/authentication but we ran into CORS problems...
// Example: https://fakerapi.it/api/v1/persons?_quantity=1&_gender=male&_birthday_start=2005-01-01

let url = "https://fakerapi.it/api/v1/persons?_quantity=1"; // start of url
let gender;                     // for gender input
let minAge;                     // for date of birth input
let request;                    // the full API
let romanticFont, heartIMG;     // preload
let dropdown, slider, button;   // for DOM elements

let stopDraw = false;           // for if-else statement in Draw function (this was a fix)

// variables for to store data from the API:
let matchIMG, firstname, lastname, birthYear, dob, profilePic, email, phone;

function preload() {
  romanticFont = loadFont('assets/MissFajardose.ttf');  // load romantic font
  heartIMG = loadImage('assets/heart.png');             // load img for frontpage
}

function setup() {
  createCanvas(800, 800);
  background(153, 0, 0);  // "romantic red"
  elements();             // DOM elements
}

// function loaned from API example in Aesthetic Programming book:
function fetchData() {
  request = url + "&_gender=" + gender + "&_birthday_start=" + birthYear + "-01-01callback=getData"; // this is the full API url
  console.log("full url: " + request);
  loadJSON(request, getData);
}

// callback function:
function getData(match) {
  console.log(match);

  // assign the data received from API to variables
  firstname = match.data[0].firstname;
  lastname = match.data[0].lastname;
  dob = match.data[0].birthday;
  profilePic = match.data[0].image;
  email = match.data[0].email;
  phone = match.data[0].phone;
}

// draw function:
function draw() {
  background(153, 0, 0);

if (stopDraw == false) {  // if (while) stopDraw = false, show the frontpage
  frontPage();
} else {                  // as soon as dropDraw = true, hide DOM elements and show your "match"
  button.hide();
  dropdown.hide();
  slider.hide();
  //  noLoop();           // not necessary

  textFont('Helvetica');  // defaults for text
  textAlign(CENTER);
  fill('white');

  // headline:
  push();
    textAlign(LEFT, TOP);
    textFont(romanticFont);
    textSize(120);
    text("Here's your match", 50, 50);
  pop();

  // img text:
  push();
    textSize(30);
    text(firstname + " " + lastname, width/2, height/3);
    textSize(14);
    text(firstname + "'s profile picture", width/2, height/1.85);
  pop();

  // match information/data:
  textSize(20);
  text("Here's some information about your match...", width/2, height/1.6);

  push();
    textSize(16);
    text("Gender: " + gender, width/2, height/1.45);
    text("Birthday: " + dob, width/2, height/1.38);
    text("Phone number: " + phone, width/2, height/1.31);
    text("Email address: " + email, width/2, height/1.24);
  pop();

  text("Reach out to your match and find eternal love <3 <3", width/2, height/1.1);

  // img of match:
  matchIMG = createImg(profilePic);             // create img as DOM element. For some reason, we had to do it like this...
  matchIMG.size(160, 120);                      // loadImage did not work
  matchIMG.position(width/2 - 80, height/2.6);
  }
}

// this function controls all the DOM elements we use:
function elements() {
  // dropdown for gender selection:
  dropdown = createSelect([82]);
  dropdown.position(150, 500);
  dropdown.option("male");  // only option here will be male/female
  dropdown.option("female");
  dropdown.style("color", "red");

  // slider for age selection:
  slider = createSlider(18, 100, 25); // range is min. 18 years of age, max. 100 years
  slider.position(190, 570);

  // button for finding love:
  button = createButton("Find my match <3");
  button.position(200, 680);
  button.style("height", "40px");
  button.style("color", "red");
}

// this function contains frontpage styling:
function frontPage() {
  textAlign(LEFT, TOP);
  fill('white');

  push();
    textFont(romanticFont);

    push();
      textSize(120);
      text("Find your soulmate", 50, 50); // headline
    pop();

    textSize(60);
    text("Search your heart, search your soul", 50, 250); // subheading
  pop();

  image(heartIMG, 380, 280, 400, 500);  // display heart img

  textFont('Helvetica');
  push();
    textSize(20);
    text("What are you searching for?", 80, 400);
  pop();

  textSize(16);
  text("Gender:", 80, 502);
  text("Minimum age:", 80, 572)
  text(slider.value() + " years", 330, 572);

  button.mousePressed(hereIsYourMatch); // if button is pressed, proceed to hereIsYourMatch()
}

// to proceed from frontpage:
function hereIsYourMatch() {
  stopDraw = true;  // jump down to 'else' in the if-else statement in draw() function
  background(153, 0, 0);  // same bg
  console.log("gender: " + dropdown.value());     // print chosen gender + min. age
  console.log("minimum age: " + slider.value());  // in console, for debugging purposes

  minAge = slider.value();      // assign slider value to 'minAge' variable
  gender = dropdown.value();    // assign choice of gender to the 'gender' variable

  birthYear = floor(random(1921, (2021 - (minAge)))); // calculate birthYear of the match

  fetchData(); // fetch the data from API, by inserting gender + birthYear in the 'request' URL
}
