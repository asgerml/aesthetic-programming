// heavily inspired by Google Chrome dinosaur game, Super Mario, and
// https://www.youtube.com/watch?v=l0HoJHc-63Q&ab_channel=TheCodingTrain
// https://github.com/bmoren/p5.collide2D/blob/master/p5.collide2d.min.js

// img variables
let marioIMG;
let bgIMG;
let shellIMG;

// sound variables
let jumpMP3;
let gameOverMP3;

let shells = [];  // array with shells
let score = 0;    // score count

// preload all the stuff
function preload() {
  bgIMG = loadImage("assets/background.jpg");
  marioIMG = loadImage("assets/mario.gif");
  shellIMG = loadImage("assets/shell.png");
  jumpMP3 = loadSound("assets/jumpSound.mp3");
  gameOverMP3 = loadSound("assets/marioDies.wav");
}

function setup() {
  createCanvas(1600, 900);
  mario = new Mario();     // create a mario
}

function draw() {
  background(bgIMG);
  textAlign(CENTER);
  textStyle(BOLD);

  shellSpawn();
  gameRules();
  displayScore();

  score++;  // add to score
}

// this function makes the shells spawn on screen
function shellSpawn() {
  if (random(1) < 0.01) {    // 1 % of the time
    shells.push(new Shell()); // push shell object into array = new shell appears on screen
  }
}

// this function contains the rules
function gameRules() {
  for (let s of shells) { // for all of the shell objects that exists in array
    s.move(); // move them
    s.show(); // show them

    if (mario.hits(s)) {  // if mario hits any of the shell objects
      console.log("game over");
      textSize(50);
      fill('red');
      text("Game over!\nRefresh page to try again...", width/2, height/3);

      gameOverMP3.play();       // play game over sound
      jumpMP3.disconnect();     // disconnects jump sound if game is over
      noLoop();                 // stop game
    }
  }

  mario.move(); // move mario
  mario.show(); // show mario
}

// this function makes mario jump on 'space' clicks
function keyPressed() {
  if (keyCode === 32) {
    mario.jump();
    jumpMP3.play(); // play the jump sound

    console.log("JUMP!");
  }
}

// this function displays the score on screen
function displayScore() {
  fill('white');
  textSize(40);
  text("Your score: " + score, width / 2 , height / 10);
}
