class Shell {

  constructor() {
    this.speed = 8;             // move shell 8 pixels per frame
    this.r = 60;                // size of shell
    this.posX = width;          // spawn pos on x axis for shells (at the width of frame)
    this.posY = height / 1.285; // spawn pos on y axis (align with the base)
  }

  move() {
    this.posX -= this.speed;  // moving rule for shells
  }

  show() {
    image(shellIMG, this.posX, this.posY, this.r * 1.33, this.r); // use shell img
  }
}
