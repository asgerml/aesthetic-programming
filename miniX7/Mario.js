class Mario {
  constructor() {
    this.r = 125;               // size of mario
    this.posX = 50;             // x position
    this.posY = height / 1.41;  // y position
    this.vy = 0;                // velocity of speed on y axis
    this.gravity = 1.5;         // gravity that counters jumps
  }

  jump() {
    // should only be possible to jump if mario touches bottom
    if (this.posY == height / 1.41) {
      this.vy = -28;  // "pushes" mario up from the bottom
    }
  }

  hits(shell) {
    // create circle around mario
    let x1 = this.posX + this.r * 0.5;
    let y1 = this.posY + this.r * 0.5;

    // create circle around shell
    let x2 = shell.posX + shell.r * 0.8;
    let y2 = shell.posY + shell.r;

    // use the collide library: if these 2 circles collide, then
    return collideCircleCircle(x1, y1, this.r, x2, y2, shell.r);  // return true if mario collides with a shell
  }

  move() {
    this.posY += this.vy;     // move mario up on screen when jumping
    this.vy += this.gravity;  // pull him down again (because of gravity)

    // set constraint, mario cannot go lower than base of map
    this.posY = constrain(this.posY, 0, height / 1.41); // when mario reaches the base, make him stay there
  }

  show() {
    image(marioIMG, this.posX, this.posY, this.r, this.r);  // use mario gif
  }
}
