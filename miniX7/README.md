# miniX7: Games with objects

<img src="screenshot1.jpg" width="600">
<br/>
<br/>
<img src="screenshot.jpg" width="600">
<br/>
Screenshots of my miniX7

## Links

- [Sketch file](https://gitlab.com/asgerml/aesthetic-programming/-/blob/master/miniX7/sketch.js)
- [Mario class](https://gitlab.com/asgerml/aesthetic-programming/-/blob/master/miniX7/Mario.js)
- [Shell class](https://gitlab.com/asgerml/aesthetic-programming/-/blob/master/miniX7/Shell.js)
- [RunMe URL](https://asgerml.gitlab.io/aesthetic-programming/miniX7/)


## Description
This week we were asked to create a simple game.
I immediately thought of the Chrome Dino Game [[1]](chrome://dino/), as I have played it way too much when I have been bored and have not had anything else to do.
I did some research and found a Coding Train video [[2]](https://www.youtube.com/watch?v=l0HoJHc-63Q&ab_channel=TheCodingTrain) about a copy of the game, so I decided to watch it.
I ended up using a lot of the code from the video example, including a library (the collide2D library by Ben Moren) [[3]](https://github.com/bmoren/p5.collide2D/blob/master/p5.collide2d.min.js) that Daniel Shiffman referred to in his video.

The game is basically just the Chrome Dino Game but instead of a dinosaur I have used Mario from the Super Mario games, and instead of cacti, Mario has to dodge turtle shells just like in the original games.
The program consists of three `.js` files: The familiar `sketch` file, and then two classes, a `Mario` class and a `Shell` class.
The `Mario` class is used to create the Mario player character in `setup` in the `sketch` file.
The `Shell` class is used to create the turtle shell objects that are randomly generated in the `sketch` file.
They will randomly be pushed into an array and then displayed on the screen as obstacles moving towards Mario for him to jump over them (they spawn 1% of the time a `random()`). It looks like this in the code:
```js
// this function makes the shells spawn on screen
function shellSpawn() {
  if (random(1) < 0.01) {     // 1 % of the time
    shells.push(new Shell()); // push shell object into array = new shell appears on screen
  }
}
```
In the `Mario` class I have used the Collide2D library the same way that Shiffman used it in his video.
By creating two ellipses that represents the outer border of both the Mario object as well as the shell objects, the `collideCircleCircle()` function is used to return `true` as soon as these circles will collide with each other.
When they do, the game will end, as it means that Mario has (been) hit by one (or more) of the shells.

The use of classes makes it easy to spawn all of the shells (the objects). This is because the class works as a blueprint for creating the same "types" of objects. Thus, it can be said that the class contains all of the characteristics and behavior of the objects...
In OOP, every single object is called an "instance" of the class, and the characteristics of these are what we call "properties" or "attributes", while we call the behavior of the objects "methods" or "functions" [5].


## Reflection
> *In learning to program, even at
the higher level, we engage in the politics of this movement between abstract and concrete
reality which is never a neutral process.* [Soon & Cox, p. 146]

When choosing what a class should contain (characteristics and behavior), we, as programmers, have to deal with an abstraction of the real world. This means that we often end up (or simply have to) simplify and generalize in programming, as the real world is far to complex and difficult to capture in (rather simple) computer programs.
Ultimately, this (again, often) leads to a more "flat" and one-dimensional representation of whatever we want to depict or process, which means that, as the quote above states, object-oriented programming is not a "neutral process".
This more or less means that OOP is a process of subjective filtering...

An example to describe this matter could very well be the example that have been used in class this week.
If we have to create a program with a human/person class, what qualities are we going to filter out? What defines a human/person? A human/person is far to complicated and complex to boil down to just 8 characteristics/properties (as done in the *Aesthetic Programming* course book on page 147) [5].

This is also why, as it is written on the same page, that:
> *Crutzen and Kotkamp argue that OOP is based on “illusions of objectivity and neutrality of
representation,” in which “[a]bstractions are simplified descriptions with a limited number of
accepted properties. They reply on the suppression of a lot of other aspects of the world.” The understanding is that objects in the real world are highly complex and nonlinear, such
abstracting and translating processes involve decision making to prioritize generalization
while less attention is paid on differences.* [Soon & Cox, p. 147]

OOP is (often) a direct simplification of something otherwise complex from the real world, which might be the reason why it probably is as good as impossible to create software that are completely objective...


## Inspiration and references
##### Inspiration
* [[1] The Chrome Dino Game](chrome://dino/): Write `chrome://dino/` in URL field in Chrome
* [[2] The Coding Train](https://www.youtube.com/watch?v=l0HoJHc-63Q&ab_channel=TheCodingTrain): By Daniel Shiffman
* [[3] p5.js Collide2D Library](https://github.com/bmoren/p5.collide2D/blob/master/p5.collide2d.min.js): By Ben Moren
* [4] Super Mario Bros by Nintendo

##### References
* [5] Soon Winnie & Cox, Geoff, "Object abstraction", *Aesthetic Programming: A Handbook of Software Studies*, London: Open Humanities Press, 2020, pp. 143-164.
